<?PHP

	/*
	--------------------------------------------------------------------------------------------------------------------
	Progam......:	index.php
	Use for.....:	index page
	Copyright..:	Jan Paul de Frankrijker (wolfje63@bitbucket.org)
	--------------------------------------------------------------------------------------------------------------------
	Date		Remarks
	-----------	--------------------------------------------------------------------------------------------------------
	2018-10-21	- Initial version.
	--------------------------------------------------------------------------------------------------------------------
	*/

	// Header.
	header('Pragma: no-cache');
	echo "<html>";
	echo "<head>";
	echo "<title>Staples Solutions scanner API</title>";
	echo "</head>";
	echo "<body>";
	echo "<style>body {padding-left:40px; font-family: 'Arial'; font-size: 12px;}</style>";

	echo "<h1>Staples Solutions scanner API</title>";
	echo "<hr>";

	if (!isset($_SERVER['HTTP_X_FORWARDED_HOST'])){
		$url = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
	} else {
		$url = "https://".$_SERVER['HTTP_X_FORWARDED_HOST'].$_SERVER['REQUEST_URI'];
	}

	echo "<h2>Links</h2>";
	echo "<hr>";
	echo "<a href='barcode.php?apiKey=6g72gs9' target='_window'>Barcode test page</a>";
	echo "<br>";
	
	echo "<h2>Images</h2>";
	echo "<hr>";
	echo "<img src='scanner000.jpg' width='300px' title='scanning...'>";
	echo "<br>";
	echo "<br>";
	echo "<img src='scanner001.jpg' width='300px'title='order screen'>";
	echo "<br>";

	echo "</body>";
	echo "</html>";

?>