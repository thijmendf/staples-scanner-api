<?PHP

	/*
	--------------------------------------------------------------------------------------------------------------------
	Progam......:	barcode.php
	Use for.....:	Generate a page with barcodes for testing.
	Copyright..:	Jan Paul de Frankrijker (wolfje63@bitbucket.org)
	--------------------------------------------------------------------------------------------------------------------
	Date		Remarks
	-----------	--------------------------------------------------------------------------------------------------------
	2018-10-21	- Initial version.
	2018-11-15	- Removed API check.
	--------------------------------------------------------------------------------------------------------------------
	*/

	header('Pragma: no-cache');
	echo "<html>";
	echo "<head>";
		
	echo "<link href='https://fonts.googleapis.com/css?family=Libre+Barcode+39+Text' rel='stylesheet'>";
	echo "</head>";
	echo "<body>";
	echo "<style>body {padding-left:40px;} .barcode{font-family: 'Libre Barcode 39 Text', cursive; font-size: 48px;} .newline{line-height:200px;}</style>";

	$repeat=10;
	$space = "200";

	echo "<table>";
	echo "<tr>";
	echo "<td><span class='barcode'>*9999*</span></td>";
	echo "<td width='".$space."px'>&nbsp;</td>";
	echo "<td><span class='barcode'>*9998*</span></td>";
	echo "<td width='".$space."px'>&nbsp;</td>";
	echo "<td><span class='barcode'>*9997*</span></td>";
	echo "</tr>";
	echo "<tr><td colspan=5><p class='newline'>&nbsp;</p></td></tr>";
	echo "<tr>";
	echo "<td><span class='barcode'>*9996*</span></td>";
	echo "<td width='".$space."px'>&nbsp;</td>";
	echo "<td><span class='barcode'>*9995*</span></td>";
	echo "<td width='".$space."px'>&nbsp;</td>";
	echo "<td><span class='barcode'>*9994*</span></td>";
	echo "</tr>";
	echo "<tr><td colspan=5><p class='newline'>&nbsp;</p></td></tr>";
	echo "<tr>";
	echo "<td><span class='barcode'>*9993*</span></td>";
	echo "<td width='".$space."px'>&nbsp;</td>";
	echo "<td><span class='barcode'>*9992*</span></td>";
	echo "<td width='".$space."px'>&nbsp;</td>";
	echo "<td><span class='barcode'>*9991*</span></td>";
	echo "</tr>";
	echo "<tr><td colspan=5><p class='newline'>&nbsp;</p></td></tr>";
	echo "<tr>";
	echo "<td><span class='barcode'>*1*</span></td>";
	echo "<td width='".$space."px'>&nbsp;</td>";
	echo "<td><span class='barcode'>*2*</span></td>";
	echo "<td width='".$space."px'>&nbsp;</td>";
	echo "<td><span class='barcode'>*5*</span></td>";
	echo "</tr>";
	echo "</table>";

	echo "<p class='newline'><hr/></p><br>";

	echo "<table>";
	echo "<tr>";
	echo "<td><img src='https://order.staplesadvantage.nl/peo2/EasyOrderStandard/65/87/asset.206587.jpg' title='850216'><br><br><span class='barcode'>*850216*</span></td>";
	echo "<td width='".$space."px'>&nbsp;</td>";
	echo "<td><img src='https://order.staplesadvantage.nl/peo2/EasyOrderStandard/65/85/asset.206585.jpg' title='850218'><br><br><span class='barcode'>*850218*</span></td>";
	echo "<td width='".$space."px'>&nbsp;</td>";
	echo "<td><img src='https://order.staplesadvantage.nl/peo2/EasyOrderStandard/54/96/asset.4255496.jpg' title='715724'><br><br><span class='barcode'>*715724*</span></td>";
	echo "</tr>";
	echo "<tr><td colspan=5><p class='newline'>&nbsp;</p></td></tr>";
	echo "<tr>";
	echo "<td><img src='https://order.staplesadvantage.nl/peo2/EasyOrderStandard/06/20/asset.2480620.jpg' title='339719'><br><br><span class='barcode'>*339719*</span></td>";
	echo "<td width='".$space."px'>&nbsp;</td>";
	echo "<td><img src='https://order.staplesadvantage.nl/peo2/EasyOrderStandard/49/65/asset.204965.jpg' title='321778'><br><br><span class='barcode'>*321778*</span></td>";
	echo "<td width='".$space."px'>&nbsp;</td>";
	echo "<td><img src='https://order.staplesadvantage.nl/peo2/EasyOrderStandard/43/62/asset.204362.jpg' title='326014'><br><br><span class='barcode'>*326014*</span></td>";
	echo "</tr>";
	echo "<tr><td colspan=5><p class='newline'>&nbsp;</p></td></tr>";
	echo "<tr>";
	echo "<td><img src='https://order.staplesadvantage.nl/peo2/EasyOrderStandard/91/60/asset.199160.jpg' title='680230'><br><br><span class='barcode'>*680230*</span></td>";
	echo "<td width='".$space."px'>&nbsp;</td>";
	echo "<td><img src='https://order.staplesadvantage.nl/peo2/EasyOrderStandard/66/86/asset.206686.jpg' title='820503'><br><br><span class='barcode'>*820503*</span></td>";
	echo "<td width='".$space."px'>&nbsp;</td>";
	echo "<td><img src='https://order.staplesadvantage.nl/peo2/EasyOrderStandard/08/80/asset.5640880.jpg' title='841778'><br><br><span class='barcode'>*841778*</span></td>";
	echo "</tr>";
	echo "<tr><td colspan=5><p class='newline'>&nbsp;</p></td></tr>";
	echo "<tr>";
	echo "<td><img src='https://order.staplesadvantage.nl/peo2/EasyOrderStandard/20/50/asset.202050.jpg' title='840906'><br><br><span class='barcode'>*840906*</span></td>";
	echo "<td width='".$space."px'>&nbsp;</td>";
	echo "<td><img src='https://order.staplesadvantage.nl/peo2/EasyOrderStandard/20/52/asset.202052.jpg' title='380615'><br><br><span class='barcode'>*380615*</span></td>";
	echo "<td width='".$space."px'>&nbsp;</td>";
	echo "<td><img src='https://order.staplesadvantage.nl/peo2/EasyOrderStandard/66/44/asset.196644.jpg' title='312749'><br><br><span class='barcode'>*312749*</span></td>";
	echo "</tr>";
	echo "<tr><td colspan=5><p class='newline'>&nbsp;</p></td></tr>";
	echo "<tr>";
	echo "<td><img src='https://order.staplesadvantage.nl/peo2/EasyOrderStandard/34/72/asset.253472.jpg' title='740510'><br><br><span class='barcode'>*740510*</span></td>";
	echo "<td width='".$space."px'>&nbsp;</td>";
	echo "<td><img src='https://order.staplesadvantage.nl/peo2/EasyOrderStandard/40/70/asset.214070.jpg' title='461009'><br><br><span class='barcode'>*461009*</span></td>";
	echo "<td width='".$space."px'>&nbsp;</td>";
	echo "<td><img src='https://order.staplesadvantage.nl/peo2/EasyOrderStandard/03/71/asset.210371.jpg' title='322177'><br><br><span class='barcode'>*322177*</span></td>";
	echo "</tr>";
	echo "</table>";

	echo "<p class='newline'>&nbsp;</p><br>";

	echo "</body>";
	echo "</html>";

?>