<?PHP

	/*
	--------------------------------------------------------------------------------------------------------------------
	Progam......:	functions.php
	Use for.....:	Fucntions for the API.
	Copyright..:	Jan Paul de Frankrijker (wolfje63@bitbucket.org)
	--------------------------------------------------------------------------------------------------------------------
	Date		Remarks
	-----------	--------------------------------------------------------------------------------------------------------
	2018-10-21	- Initial version.
	2018-10-26	- Added additional database for logging.
				- Check if the API Key is revoked.
				- Added product with the cabinets. So a cabinet can have a subset of products.
	2018-11-09	- Added ponumber and requestpo.
	2018-11-14	- Added test indicator in APIKEY.
	2018-11-18	- Changed JSON cabinets and products.
				- Test indicator in the checkApiKey is no longer needed.
	2018-11-21	- Add getNews.
	2018-12-10	- Added contact information per cabinet.
	2018-12-11	- Added price, min and max to cabinet.
	2018-12-13	- Changed PO number logic.
	2018-12-16	- All messages are for now in Dutch.
	2019-01-20	- Added reason codes to help with translations.
                - Added version check of the app.
    2019-01-30  - Only send products that are active.
    			- Add picture url in the products api (thumbnail url).
    2019-02-05	- Added image url lookup.
    2019-02-18	- Added orderhistory API.
    2019-02-25  - Added orderlinestatus with orderhistory API.
	--------------------------------------------------------------------------------------------------------------------
	*/

	// SQLite data database.
	class MyDB1 extends SQLite3{
		function __construct(){
			$this->open("data.sqlite");
		}
	}
	$db = new MyDB1();

	// SQLite Orders database.
	class MyDB2 extends SQLite3{
		function __construct(){
			$this->open("orders.sqlite");
		}
	}
	$dbord = new MyDB2();

	// Check the API key.
	function checkApiKey($apiKey, &$db){
		$response = Array("status"=>false,"remark"=>"Staples API Key JSON");
		$result = $db->query("SELECT * FROM apikeys WHERE apikey='$apiKey' LIMIT 1;");
		if (!$result) {
			$response['remark'] = "Staples API Key";
			$response['reason'] = "Geen API Key gevonden.";
			return $response;
		}
		$row = $result->fetchArray();
		if(!empty($row)){
			if ($row['valid']){
				$response['status'] = true;
				$response['id']=$row['id'];
				$response['name']=$row['name'];
			} else {
				$response['reason'] = "API Key ingetrokken.";
				$response['reasoncode'] = 0;
			}
		} else {
			$response['reason'] = "API Key is niet geldig.";
			$response['reasoncode'] = 1;
		}
		return $response;
	}

    // Get the cabinet against the appkeyID.
    function checkCabinet($appkeyid, $cabinet, &$db){
        $response =  Array("status"=>false,"remark"=>"Staples orderhistory cabinet check");
        $result = $db->query("SELECT * FROM links WHERE appkeyid=$appkeyid AND cabinetid=$cabinet AND valid=1 LIMIT 1;");
        if (!$result) {
            $response['reason'] = "Geen kasten gevonden voor deze API Key.";
            return $response;
        } else {
			$row = $result->fetchArray();
			if(!empty($row)){
            	$response['status'] = true;
            } else {
            	$response['reason'] = "Geen kasten gevonden voor deze API Key.";
            	return $response;
            }
        }
        return $response;
    }

	// Get the cabinets for the appkeyID.
	// Also get the products that can be ordered in the cabinet.
	function getCabinets($appkeyid, &$db){
		$response =  Array("status"=>false,"remark"=>"Staples cabinet JSON","data"=>Array());
		$result = $db->query("SELECT * FROM links WHERE appkeyid=$appkeyid AND valid=1;");
		if (!$result) {
			$response['reason'] = "Geen kasten gevonden voor deze API Key.";
			$response['reasoncode'] = 2;
			return $response;
		}
		$links = Array();
		while ($row = $result->fetchArray()){
			$cabinets[]=$row['cabinetid'];
		}
		if(empty($cabinets)){
			$response['reason'] = "Geen kasten gekoppeld voor deze API Key.";
			$response['reasoncode'] = 3;
			return $response;
		}
		$response['status'] = true;
		foreach ($cabinets as $cabinet) {
			$result = $db->query("SELECT id, name1||' '||name2 as name, requestpo, ponumber, contact_name, contact_phone, contact_message FROM cabinets WHERE id=".$cabinet." LIMIT 1;");
			if (!$result) {
				$response['reason'] = "Geen kast(en) gevonden.";
				$response['reasoncode'] = 4;
				return $response;
			}
			$row = $result->fetchArray();
			if (!empty($row)){
				$entry = Array();
				$entry['name'] = $row['name'];
				# Check if a PO number must be asked for.
				if ($row['requestpo']) {
					$entry['requestpo'] = 1;
				}
				# Check the default PO number.
				if ($row['ponumber']) {
					$entry['ponumber'] = $row['ponumber'];
				}
				# Check the contact name.
				if ($row['contact_name']) {
					$entry['contact_name'] = $row['contact_name'];
				}
				# Check the contact phone number.
				if ($row['contact_phone']) {
					$entry['contact_phone'] = $row['contact_phone'];
				}
				# Check the message that needs to be send to the user.
				if ($row['contact_message']) {
					$entry['contact_message'] = $row['contact_message'];
				}
				$result2 = $db->query("SELECT * FROM cabinet_content WHERE cabinet=".$cabinet." ORDER BY productcode;");
				while ($row2 = $result2->fetchArray()){
					$entry2 = Array();
					if ($row2['price']) {
						$entry2['price'] = $row2['price'];
					}
					if ($row2['minstock']) {
						$entry2['min'] = $row2['minstock'];
					}
					if ($row2['maxstock']) {
						$entry2['max'] = $row2['maxstock'];
					}
					$entry['products'][$row2['productcode']] = $entry2;
				}
				$response['data'][$row['id']] = $entry;
			}
		}
		return $response;
	}

	// Get all the products.
	function getProducts($cabinetID, &$db){
		$response = Array("status"=>false,"remark"=>"Staples product JSON","data"=>Array());
		$result = $db->query("SELECT productcode, name, status FROM products ORDER BY productcode;");
		if (!$result) {
			$response['reason'] = "Geen producten gevonden.";
			$response['reasoncode'] = 5;
			return $response;
		}
		$response['status'] = true;
		while ($row = $result->fetchArray()){
			$entry = Array();
			$entry['name'] = $row['name'];
			$entry['status'] = $row['status'];
			$response['data'][$row['productcode']] = $entry;
		}
		return $response;
	}

	// Get the image url.
	function getImageURL($productcode, &$db){
		$result = $db->query("SELECT url FROM products where productcode=".$productcode.";");
		if (!$result) {
			return false;
		}
		while ($row = $result->fetchArray()){
			return $row['url'];
		}
	}

	// Get all the ean codes.
	function getEAN13($cabinetID, &$db){
		$response = Array("status"=>false,"remark"=>"Staples EAN13 JSON","data"=>Array());
		$result = $db->query("SELECT ean13, productcode FROM ean13 ORDER BY ean13, productcode;");
		if (!$result) {
			$response['reason'] = "Geen EAN code(s) gevonden.";
			return $response;
		}
		$response['status'] = true;
		while ($row = $result->fetchArray()){
			$entry = Array();
			$entry['articleID'] = $row['productcode'];
			$response['data'][$row['ean13']] = $entry;
		}
		return $response;
	}

	// Get the news item.
	function getNews($version, &$db){
		$response = Array("status"=>true,"remark"=>"Staples News JSON","header"=>Array(),"data"=>Array());
        $result = $db->query("SELECT * FROM appversion ORDER BY version DESC LIMIT 1;");
        while ($row = $result->fetchArray()){
            $appversion = $row['version'];
            $applink = $row['url'];
            $appnews = $row['news'];
        }
        if ($version < $appversion){
            $response['header'] = "<h3>ATTENTIE</h3>U gebruikt een oude versie van de app. Er is een nieuwe versie (".$appversion .") te downloaden via de Google Playstore of Apple Appstore.";
            $response['data'] = array($appnews);
        } else {
    		$result = $db->query("SELECT * FROM news ORDER BY id;");
    		while ($row = $result->fetchArray()){
    			if ($row['id']==0){
    				$response['header'] = $row['news'];
    			} else {
    				$response['data'][] = $row['news'];
    			}
    		}
        }
		return $response;
	}

	// Get the orderhistory.
	function getOrderhistory($cabinet, &$dbord){
		$response = Array("status"=>false,"remark"=>"Staples orderhistory JSON","data"=>Array());
		$result = $dbord->query("SELECT * FROM orderlines where cabinet=".$cabinet." ORDER BY orderid DESC, line ASC;");
		if (!$result) {
			$response['reason'] = "Geen oude orders gevonden.";
			return $response;
		}
		$response['status'] = true;
		$orderid=0;
		while ($row = $result->fetchArray()){
			if ($orderid != $row['orderid']) {
				$response['data'][$row['orderid']]['cabinet'] = $row['cabinet'];
			}
            $response['data'][$row['orderid']][$row['line']]['productcode'] = $row['productcode'];
            $response['data'][$row['orderid']][$row['line']]['quantity'] = $row['quantity'];
            $response['data'][$row['orderid']][$row['line']]['orderlinestatus'] = $row['status'];
            $orderid = $row['orderid'];
		}
		return $response;
	}

?>
