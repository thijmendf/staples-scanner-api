<?PHP

    /*
    --------------------------------------------------------------------------------------------------------------------
    Progam......:   requestCode.php
    Use for.....:   Request an apiKey.
    Copyright..:    Jan Paul de Frankrijker (wolfje63@bitbucket.org)
    --------------------------------------------------------------------------------------------------------------------
    Date        Remarks
    ----------- --------------------------------------------------------------------------------------------------------
    2019-06-03  - Initial version.
    --------------------------------------------------------------------------------------------------------------------
    */

    // JSON header.
    header('Pragma: no-cache');
    header("Content-Type: application/json");
    header("HTTP/1.1 200 OK");

    // Include functions file.
    include_once("functions.php");

    // Get the data from the send request.
    $requestdata = json_decode(file_get_contents('php://input'), true);

    // Write log file.
    file_put_contents("log/request_".date('Ymd_His').gettimeofday()['usec'].".json", json_encode($requestdata));

    // Initialize the reponse array.
    $totalresponse = Array();

    // Loop through the order lines.
    foreach ($requestdata as $request) {

        // Get the generic values.
        $response = Array();
        $response['status'] = "Geachte ".$request['data']['name'].".\n Uw aanvraag is in goede orde ontvangen en wij nemen zo snel mogelijk contact met u op.\nMet vriendelijke groet,\nStaples Advantage Benelux.";
        $totalresponse[]=$response;

        // Write request into database.
        if ($request['data']['subscribeNews'] === true) {
            $news = "Yes";
        } else {
            $news = "No";
        }
        $statement = $dbord->prepare("INSERT INTO requests (name,company,account,kvk,email,news,version,platform) VALUES('".$request['data']['name']."','".$request['data']['organization']."',".$request['data']['debitNumber'].",'".$request['data']['kvkNumber']."','".$request['data']['email']."','".$news."','".$request['version']."','".$request['platform']."');");
        $insertresult = $statement->execute();
    }

    // Create the JSON response.
    echo json_encode($totalresponse);

?>
