<?PHP

	/*
	--------------------------------------------------------------------------------------------------------------------
	Progam......:	news.php
	Use for.....:	Generate a JSON file with a news message.
	Copyright..:	Jan Paul de Frankrijker (wolfje63@bitbucket.org)
	--------------------------------------------------------------------------------------------------------------------
	Date		Remarks
	-----------	--------------------------------------------------------------------------------------------------------
	2018-11-21	- Initial version.
	2019-01-20	- Added reason codes to help with translations.
				- Added version check of the app.
	--------------------------------------------------------------------------------------------------------------------
	*/

	// JSON header.
	header('Pragma: no-cache');
	header("Content-Type: application/json");

	// Include functions file.
	include_once("functions.php");

	// Check if the APIKEY is set. If not return error.
	if (!isset($_REQUEST['apiKey'])) {
		$response = Array("status"=>false);
		$response['reason'] = "API Key niet ingegeven.";
		$response['reasoncode'] = 6;
		echo json_encode($response);
		exit;
	}
	// Get the APIKEY.
	$apiKey = isset($_POST['apiKey']) ? $_POST['apiKey'] : $_GET['apiKey'];

	// Get the APPVERSION
	if (!isset($_REQUEST['appversion'])) {
		$appversion = "0.0.10";
	} else {
		$appversion = isset($_POST['appversion']) ? $_POST['appversion'] : $_GET['appversion'];
	}

	// Check the APIKEY and return JSON response with the status of the key if there is an error.
	$status=checkApiKey($apiKey, $db);
	if (!$status['status']){
		echo json_encode($status);
		exit;
	}

	// Get the cabinet information and return JSON response.
	echo json_encode(getNews($appversion,$db), true);

?>