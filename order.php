<?PHP

	/*
	--------------------------------------------------------------------------------------------------------------------
	Progam......:	order.php
	Use for.....:	Generate an XML file from a given order JSON file.
	Copyright..:	Jan Paul de Frankrijker (wolfje63@bitbucket.org)
	--------------------------------------------------------------------------------------------------------------------
	Date		Remarks
	-----------	--------------------------------------------------------------------------------------------------------
	2018-10-21	- Initial version.
	2018-10-22	- Added logging into database.
				- Added reponses when the generation of the file is unsuccessfull.
				- Create a seperate file per order in the JSON file..
				- Create a seperate response per order in the JSON file.
	2018-11-05	- Added XML tags <HEADER_TEXT1/> until <HEADER_TEXT5/>, <TOTAL_VALUE/> and <CURRENCY>EUR</CURRENCY> in <HEADER>.
	2018-11-08	- If PO number is setup in database, use that one.
	2018-11-09	- If PO number is send from the order, use that one, overwrite anything that is setup in the database.
	2018-11-13	- Added rename in the last part of the file to prevent reading/writing at the same time.
	2018-11-14	- Check if the file is a valid JSON file, if not send back an error.
	2018-11-27	- If an order is already received and in the DB (timestamp=orderid), then do not process the order.
	2018-12-16	- If an order is already received and in the DB (timestamp=orderid), then send back status=true so the order is being deleted from the app.
				- All messages are for now in Dutch.
	2018-12-17	- Added appversion.
	2018-12-20	- Check on test was incorrect.
	2019-01-20	- Added reason codes to help with translations.
    2019-02-18  - Added orderlines into the orders database.
    2019-02-15  - Add default orderstatus to the orders database.
    2019-05-01  - Add logfile for logging purposes.
    			- If the cabinet is not ok, do not process the order. However continue processing.
    2019-05-02  - If the cabinet number is empty, just send an ok, however do not process the order.
    2019-06-03	- Stop extra logging for now.
	--------------------------------------------------------------------------------------------------------------------
	*/

	// JSON header.
	header('Pragma: no-cache');
	header("Content-Type: application/json");
	header("HTTP/1.1 200 OK");

	// Include functions file.
	include_once("functions.php");

	// Get the orderdata from the send file.
	$orderdata = json_decode(file_get_contents('php://input'), true);

    // // Write log file.
    // file_put_contents("log/".date('Ymd_His').gettimeofday()['usec'].".json", json_encode($orderdata));

	// Initialize the reponse array.
	$totalresponse = Array();

	// Loop through the order lines.
	foreach ($orderdata as $orders) {

		// Get the generic values.
		$apikey = $orders['apiKey'];
		$cabinet = $orders['cabinetID'];
		$timestamp = $orders['timestamp'];
		if (isset($orders['version'])) {
			$appversion = $orders['version'];
		} else {
			$appversion = NULL;
		}

		// Check if the APIKEY is valid.
		$result = $db->query("SELECT apikey FROM apikeys WHERE apikey='".$apikey."' LIMIT 1;");
		$row = $result->fetchArray();
		if(empty($row)){
			$response = Array("status"=>false);
			$response['reason'] = "API Key is ongeldig.";
			$response['reasoncode'] = 6;
			$response['timestamp'] = $timestamp;
			$totalresponse[]=$response;
			break;
		}

		// check if the cabinet is not null.
		if (is_null($cabinet)){
			$response = Array("status"=>True);
			$response['reason'] = "Kast nummer is leeg.";
			$response['reasoncode'] = 9;
			$response['timestamp'] = $timestamp;
			$totalresponse[]=$response;

		} else {

			// Get the address information for the cabinet from the database.
			$result = $db->query("SELECT * FROM cabinets WHERE id=".$cabinet." LIMIT 1;");
			$row = $result->fetchArray();
			if(!empty($row)){

				// Loop through the order lines.
				if(!empty($orders['articles'])){

					// Check if the order is already in the order database.
					// If already in the DB, ignore the order.
					$result2 = $dbord->query("SELECT * FROM orders WHERE orderid=".$timestamp." AND cabinet=".$cabinet." LIMIT 1;");
					$row2 = $result2->fetchArray();
					if(!empty($row2)){
						$statement = $dbord->prepare("INSERT INTO orders VALUES('Order already processed',NULL,'".$timestamp."',".$cabinet.",'".$apikey."',NULL,'".$appversion."');");
						$insertresult = $statement->execute();
						$response = Array("status"=>true);
						$response['reason'] = "Order is reeds eerder verzonden.";
						$response['reasoncode'] = 7;
						$response['timestamp'] = $timestamp;
						$totalresponse[]=$response;
					} else {

						// Process the JSON file.
						$file = date('Ymd_His').gettimeofday()['usec'];
						$filepath = "request/".$file;
						$xmlfile = "<XMLMESSAGE>\n";
						$xmlfile .= "\t<ORDER>\n";
						// Add test indicator if needed.
						if ($row['test'] == 1){
							$xmlfile .= "\t\t<TEST>YES</TEST>\n";
						}
						$xmlfile .= "\t\t<HEADER>\n";
						$xmlfile .= "\t\t\t<ACCOUNT_NUMBER>".$row['account']."</ACCOUNT_NUMBER>\n";
						$xmlfile .= "\t\t\t<SALES_DEPARTMENT>".$row['companycode']."</SALES_DEPARTMENT>\n";
						$xmlfile .= "\t\t\t<DELIVERY_ADRESS>\n";
						$xmlfile .= "\t\t\t\t<NAME1>".htmlspecialchars($row['name1'])."</NAME1>\n";
						$xmlfile .= "\t\t\t\t<NAME2>".$row['name2']."</NAME2>\n";
						$xmlfile .= "\t\t\t\t<ADRESS1>".$row['address']."</ADRESS1>\n";
						$xmlfile .= "\t\t\t\t<POSTCODE>".$row['postcode']."</POSTCODE>\n";
						$xmlfile .= "\t\t\t\t<TOWN>".$row['town']."</TOWN>\n";
						$xmlfile .= "\t\t\t\t<COUNTRY>".$row['country']."</COUNTRY>\n";
						$xmlfile .= "\t\t\t</DELIVERY_ADRESS>\n";
						$xmlfile .= "\t\t\t<ORDER_DATE>".date('Ymd')."</ORDER_DATE>\n";
						// Add the PO number from the database.
						if ($row['ponumber'] == Null){
							// If the PO number is not ot given, create one.
							$ponumber = $row['account']."-".$cabinet."-".date('Ymd');
						} else {
							$ponumber = $row['ponumber'];
						}
						// If a PO number is given in the order itself, use that one.
						if (isset($orders['ponumber'])) {
							$ponumber = $orders['ponumber'];
						}
						$xmlfile .= "\t\t\t<ORDER_NUMBER>".$ponumber."</ORDER_NUMBER>\n";
						$xmlfile .= "\t\t\t<ORDERED_BY>SCANNER CABINET ".$cabinet."</ORDERED_BY>\n";
						$xmlfile .= "\t\t\t<HEADER_TEXT1/>\n";
						$xmlfile .= "\t\t\t<HEADER_TEXT2/>\n";
						$xmlfile .= "\t\t\t<HEADER_TEXT3/>\n";
						$xmlfile .= "\t\t\t<HEADER_TEXT4/>\n";
						$xmlfile .= "\t\t\t<HEADER_TEXT5/>\n";
						$xmlfile .= "\t\t\t<TOTAL_VALUE/>\n";
						$xmlfile .= "\t\t\t<CURRENCY>EUR</CURRENCY>\n";
						$xmlfile .= "\t\t</HEADER>\n";
						$xmlfile .= "\t\t<LINE_ITEMS>\n";
						$counter = 0;
						// Check if the extra service product must be added.
						if ($row['extra'] > 0){
							$counter++;
							$xmlfile .= "\t\t\t<ITEM>\n";
							$xmlfile .= "\t\t\t\t<LINE>".$counter."</LINE>\n";
							$xmlfile .= "\t\t\t\t<ARTICLE_NUMBER>".$row['extra']."</ARTICLE_NUMBER>\n";
							$xmlfile .= "\t\t\t\t<ARTICLE_QUANTITY>1</ARTICLE_QUANTITY>\n";
							$xmlfile .= "\t\t\t</ITEM>\n";
	                        $statement = $dbord->prepare("INSERT INTO orderlines VALUES(NULL,".$cabinet.",'".$timestamp."',".$counter.",".$row['extra'].",1,'Gefactureerd (service artikel)');");
	                        $insertresult = $statement->execute();
						}
						// Add the products.
						foreach($orders['articles'] as $articleID=>$amount){
							$counter++;
							$xmlfile .= "\t\t\t<ITEM>\n";
							$xmlfile .= "\t\t\t\t<LINE>".$counter."</LINE>\n";
							$xmlfile .= "\t\t\t\t<ARTICLE_NUMBER>".$articleID."</ARTICLE_NUMBER>\n";
							$xmlfile .= "\t\t\t\t<ARTICLE_QUANTITY>".$amount."</ARTICLE_QUANTITY>\n";
							$xmlfile .= "\t\t\t</ITEM>\n";
	                        $statement = $dbord->prepare("INSERT INTO orderlines VALUES(NULL,".$cabinet.",'".$timestamp."',".$counter.",".$articleID.",".$amount.",'Ontvangen');");
	                        $insertresult = $statement->execute();
						}
						$xmlfile .= "\t\t</LINE_ITEMS>\n";
						$xmlfile .= "\t</ORDER>\n";
						$xmlfile .= "</XMLMESSAGE>";

						// Write the generated file in the database for logging purposes.
						$statement = $dbord->prepare("INSERT INTO orders VALUES('".$file."','".$xmlfile."','".$timestamp."',".$cabinet.",'".$apikey."',NULL,'".$appversion."');");
						$insertresult = $statement->execute();

						// Save the file.
						$saveresult = file_put_contents($filepath.".tmp", $xmlfile);

						// Check the write result.
						if (!$saveresult) {
	                        // If not ok, generate the 'oh-oh' response.
							$statement = $dbord->prepare("UPDATE orders SET written=0 WHERE filename='".$file."';");
							$insertresult = $statement->execute();
							$response = Array("status"=>false);
							$response['reason'] = "Order kan niet worden aangemaakt.";
							$response['reasoncode'] = 8;
							$response['timestamp'] = $timestamp;
							$totalresponse[]=$response;
							// Delete the temporary file.
							unlink($filepath.".tmp");
						} else {
							// If ok, generate the 'ok' response.
							$statement = $dbord->prepare("UPDATE orders SET written=".$saveresult." WHERE filename='".$file."';");
							$insertresult = $statement->execute();
							$response = Array("status"=>true);
							$response['reason'] = "Order is goed verzonden.";
							$response['reasoncode'] = 9;
							$response['timestamp'] = $timestamp;
							$response['filename'] = $file.".xml";
							$totalresponse[]=$response;
							// Rename the temporary file.
							rename($filepath.".tmp", $filepath.".xml");
						}
					}
				}
			}
		}
	}

	// Create the JSON response.
	echo json_encode($totalresponse);

?>
