<?PHP

    /*
    --------------------------------------------------------------------------------------------------------------------
    Progam......:   orderhistory.php
    Use for.....:   Get the list of orders.
    Copyright..:    Jan Paul de Frankrijker (wolfje63@bitbucket.org)
    --------------------------------------------------------------------------------------------------------------------
    Date        Remarks
    ----------- --------------------------------------------------------------------------------------------------------
    2019-02-18  - Initial version.
    --------------------------------------------------------------------------------------------------------------------
    */

    // JSON header.
    header('Pragma: no-cache');
    header("Content-Type: application/json");

    // Include functions file.
    include_once("functions.php");

    // Check if the APIKEY is set. If not return error.
    if (!isset($_REQUEST['apiKey'])) {
        $response = Array("status"=>false);
        $response['reason'] = "API Key niet ingegeven.";
        $response['reasoncode'] = 6;
        echo json_encode($response);
        exit;
    }
    // Get the APIKEY.
    $apiKey = isset($_POST['apiKey']) ? $_POST['apiKey'] : $_GET['apiKey'];

    // Check if the cabinet is set. If not return error.
    if (!isset($_REQUEST['cabinet'])) {
        $response = Array("status"=>false);
        $response['reason'] = "Geen kast opgegeven";
        echo json_encode($response);
        exit;
    }
    // Get the cabinetid.
    $cabinet = isset($_POST['cabinet']) ? $_POST['cabinet'] : $_GET['cabinet'];

    // Check the APIKEY and return JSON response with the status of the key if there is an error.
    $status = checkApiKey($apiKey, $db);
    if (!$status['status']){
        echo json_encode($status);
        exit;
    } else {
        $apikeyid = $status['id'];
    }

    // Check if the cabinet requested may be requested by the apikey.
    $status = checkCabinet($apikeyid, $cabinet, $db);
    if (!$status['status']){
        echo json_encode($status);
        exit;
    }

    // Get the product information and return JSON response.
    echo json_encode(getOrderhistory($cabinet, $dbord));

?>