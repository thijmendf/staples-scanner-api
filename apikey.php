<?PHP

	/*
	--------------------------------------------------------------------------------------------------------------------
	Progam......:	apikey.php
	Use for.....:	Get an apiKey from the a given apiKey in the URL (post or get) and check this against the checkApiKey function.
	Copyright..:	Jan Paul de Frankrijker (wolfje63@bitbucket.org)
	--------------------------------------------------------------------------------------------------------------------
	Date		Remarks
	-----------	--------------------------------------------------------------------------------------------------------
	2018-10-21	- Initial version.
	2019-01-20	- Added reason codes to help with translations.
	--------------------------------------------------------------------------------------------------------------------
	*/

	// JSON header.
	header('Pragma: no-cache');
	header("Content-Type: application/json");
	header("HTTP/1.1 200 OK");

	// Include functions file.
	include_once("functions.php");

	// Check if the APIKEY is set. If not return error.
	if (!isset($_REQUEST['apiKey'])) {
		$response = Array("status"=>false);
		$response['reason'] = "API Key niet ingegeven.";
		$response['reasoncode'] = 6;
		echo json_encode($response);
		exit;
	}
	// Get the APIKEY.
	$apiKey = isset($_POST['apiKey']) ? $_POST['apiKey'] : $_GET['apiKey'];

	// Check the APIKEY and return JSON response.
	echo json_encode(checkApiKey($apiKey, $db));

?>