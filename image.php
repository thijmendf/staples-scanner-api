<?PHP

    /*
    --------------------------------------------------------------------------------------------------------------------
    Progam......:   image.php
    Use for.....:   Get an url for a given product code.
    Copyright..:    Jan Paul de Frankrijker (wolfje63@bitbucket.org)
    --------------------------------------------------------------------------------------------------------------------
    Date        Remarks
    ----------- --------------------------------------------------------------------------------------------------------
    2019-02-05  - Initial version.
    --------------------------------------------------------------------------------------------------------------------
    */

    // Include functions file.
    include_once("functions.php");

    // Check if the product code is set. If not return nothing
    if (isset($_REQUEST['productcode'])) {
        $url = getImageURL($_REQUEST['productcode'], $db);
        if ($url){
            // Imgage header.
            header('Pragma: no-cache');
            header("Location: ".$url);
        }
    }

?>